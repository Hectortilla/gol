#!/usr/bin/env python
# -*- coding: utf-8 -*-

import random
import sys

def init_grid(x, y):
    grid = [[0] * x for _ in range(y)]
    return grid

def random_fill(grid):
    for i_row, _ in enumerate(grid):
        for i_col, _ in enumerate(grid[i_row]):
            grid[i_row][i_col] = random.randrange(2)


def display_grid(grid):
    for row in grid:
        print(row)

def get_number_life_cells_around(i_row, i_col, grid):
    max_row_size = len(grid)
    max_col_size = len(grid[0])

    if i_row == max_row_size - 1:
        pass

    if i_col == max_col_size - 1:
        pass

    number_life_cells = 0

    # - TOP LEFT
    number_life_cells += grid[i_row - 1][i_col - 1]
    # - TOP
    number_life_cells += grid[i_row - 1][i_col]
    # - TOP RIGHT
    # - RIGHT
    if i_col == max_col_size - 1:
        number_life_cells += grid[i_row - 1][0]
        number_life_cells += grid[i_row][0]
    else:
        number_life_cells += grid[i_row - 1][i_col + 1]
        number_life_cells += grid[i_row][i_col + 1]
    # - LEFT
    number_life_cells += grid[i_row][i_col - 1]
    # - BOTTOM LEFT
    # - BOTTOM
    # - BOTTOM RIGHT
    if i_row == max_row_size - 1:
        number_life_cells += grid[0][i_col - 1]
        number_life_cells += grid[0][i_col]
        if i_col == max_col_size - 1:
            number_life_cells += grid[0][0]
        else:
            number_life_cells += grid[0][i_col + 1]
    else:
        number_life_cells += grid[i_row + 1][i_col - 1]
        number_life_cells += grid[i_row + 1][i_col]
        if i_col == max_col_size - 1:
            number_life_cells += grid[i_row + 1][0]
        else:
            number_life_cells += grid[i_row + 1][i_col + 1]
    return number_life_cells
# Una célula viva con 2 o 3 células vecinas vivas sigue viva, en otro caso muere (por "soledad" o "superpoblación").
# Una célula muerta con exactamente 3 células vecinas vivas "nace" (es decir, al turno siguiente estará viva).
def cicle_life(grid):
    for i_row, _ in enumerate(grid):
        for i_col, cell_value in enumerate(grid[i_row]):
            num_life_cells_around = get_number_life_cells_around(i_row, i_col, grid)

            if cell_value:
                if num_life_cells_around not in [2, 3]:
                    grid[i_row][i_col] = 0
            else:
                if num_life_cells_around == 3:

                    grid[i_row][i_col] = 1
