from helpers import init_grid, random_fill, display_grid, cicle_life

def main_game_loop(grid):
	while True:
		display_grid(grid)
		print('--------' * 100)
		cicle_life(grid)

def run_game():

	# Inincializamos grid con ceros
	grid = init_grid(10, 10)
	# Assignamos valores aleatorios [0, 1] a el grid
	# Sergio aprende variables pasadas por referencia (la funcion no retorna valor!) y enumerate
	random_fill(grid)

	main_game_loop(grid)

if __name__ == '__main__':
	run_game()